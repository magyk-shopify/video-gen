// Interface for Script response data
interface ScriptResponse {
  scenes: string;
}

// Interface for Scene response
interface Scene {
  scene_title: string;
  scene_description: string;
  narrator: string;
}

// Interface for OpenAI API response
interface APIResponse extends Response {
  arrayBuffer(): Promise<ArrayBuffer>;
}

// Interface for image prompts
interface ImagePrompt {
  prompt: string;
  style: 'vivid' | 'natural';
}

// Interface for image response
interface ImageResponse {
  openai?: {
    url: string;
  };
}

// Interface for image prompt response
interface ImagePromptResponse {
  prompt: string;
  style: string;
}

// Interface for media files
interface Media {
  image: string; // openai URL
  audio: string;
  subtitle: string;
}

// Interface for transcription segment
interface Segment {
  // id: number;
  // seek: number;
  // start: number;
  // end: number;
  // text: string;
  // tokens: number[];
  // temperature: number;
  // avg_logprob: number;
  // compression_ratio: number;
  // no_speech_prob: number;
  word: string;
  start: number;
  end: number;
}


// Interface for transcription response
interface Transcription {
  task: string;
  language: string;
  duration: number;
  text: string;
  words: Segment[];
}

export {
  ScriptResponse,
  Scene,
  APIResponse,
  ImagePrompt,
  ImageResponse,
  ImagePromptResponse,
  Media,
  Segment,
  Transcription,
};