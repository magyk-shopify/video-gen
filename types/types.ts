import { z } from 'zod';

export const OpenAIImage = z.object({
  id: z.string().optional(),
  params: z.object({
    prompt: z.string(),
    size: z.string(),
    style: z.string(),
    quality: z.string(),
    rprompt: z.string().optional()
  }),
  openai: z.object({
    url: z.string().optional(),
    b64: z.string().optional()
  }).optional()
});

export const OpenAIImageParams = z.object({
  prompt: z.string(),
  count: z.number().optional(),
  persist: z.boolean().optional(),
  format: z.enum(['url', 'b64_json']).optional(),
  quality: z.enum(['standard', 'hd']).optional(),
  size: z.enum(['256x256', '512x512', '1024x1024', '1792x1024', '1024x1792']).optional(),
  style: z.enum(['vivid', 'natural']).optional()
});

export type OpenAIImage = z.infer<typeof OpenAIImage>;
export type OpenAIImageParams = z.infer<typeof OpenAIImageParams>;