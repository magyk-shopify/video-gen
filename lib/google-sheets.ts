import { google } from 'googleapis';

const keyFile: string = (process.env.GOOGLE_KEYFILE_PATH as string || '.') + '/googlecreds.json';

// console.log("+++++=============",keyFile);

const readOnlyAuth = new google.auth.GoogleAuth({
  keyFile: keyFile,
  scopes: ['https://www.googleapis.com/auth/spreadsheets.readonly'],
});


const readOnlySheets = google.sheets({ version: 'v4', auth: readOnlyAuth });

function extractSpreadsheetId(url: string): string | null {
  const regex = /\/d\/([a-zA-Z0-9-_]+)/;
  const match = url.match(regex);
  if (match && match[1]) {
    return match[1];
  }
  return null;
}

function getSheetUrl(url: string): string | null {
  const id = extractSpreadsheetId(url);
  if (id) {
    return `https://docs.google.com/spreadsheets/d/${id}`;
  }
  return null;
}

async function readTabFromSheetURL(url: string, tab: string): Promise<any[][]> {
  const sheetId = extractSpreadsheetId(url);
  if (!sheetId) {
    throw new Error('Error extracting spreadsheet id from url - ' + url);
  }
  const response = await readOnlySheets.spreadsheets.values.get({
    spreadsheetId: sheetId,
    range: `${tab}!A:D`
  });
  return response.data.values || [];
}

export const GoogleSheets = {
  getSheetUrl: getSheetUrl,
  readTabFromSheetURL: readTabFromSheetURL
};