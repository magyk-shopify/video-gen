import { ChatCompletionFunctionParamTypes, ChatCompletionFunctionParams, ChatCompletionRequestMessage } from './openai';

type PromptParameters = {
  [key: string]: string | number
};

function interpolatePrompt(template: string, data: any) {
  return template.replace(/{(\w+)}/g, (_, key) => {
    return (data && data[key]) || '';
  });
}

function getBlogFunctionParameters(parameters: string[][]): ChatCompletionFunctionParams {
  return parameters.reduce((result: ChatCompletionFunctionParams, row: string[]): ChatCompletionFunctionParams => {
    const [key, description] = row;
    const [param, type] = key.split('|');
    const [pType, psType] = type ? type.split('|') : ['string'];

    result.properties[param] = {
      description: description,
      type: pType as ChatCompletionFunctionParamTypes,
      items: pType === 'array' ? { type: psType as ChatCompletionFunctionParamTypes } : undefined
    };
    return result;

  }, { type: 'object', properties: {} });
}

function toCamelCase(input: string): string {
  const words = input.trim().split(' ');

  if (words.length === 1) {
    return words[0].charAt(0).toLowerCase() + words[0].slice(1);
  }

  const camelCaseWords = words.map((word, index) => {
    if (index === 0) {
      return word.toLowerCase();
    }
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
  });

  return camelCaseWords.join('');
}

export function getBlogPrompt(prompt: string[][], attributes: PromptParameters): [ChatCompletionRequestMessage[], ChatCompletionFunctionParams, ChatCompletionRequestMessage | undefined] {
  const requestMessages: ChatCompletionRequestMessage[] = [];
  const captureMessags: string[][] = [];
  let repeatingInstruction;

  prompt.shift();

  prompt.forEach((row: string[]) => {
    const [name, type, option, text] = row;
    if (!name || !type || !text) {
      return;
    }
    if (type === 'SystemMessage') {
      requestMessages.push({ role: 'system', content: text.trim() });
      return;
    }
    if (type === 'UserMessage') {
      requestMessages.push({ role: 'user', content: text.trim() });
      return;
    }
    if (type === 'AssistantMessage') {
      requestMessages.push({ role: 'assistant', content: interpolatePrompt(text.trim(), attributes) });
      return;
    }
    if (type === 'RepeatingInstruction') {
      repeatingInstruction = { role: 'user', content: text.trim() } as ChatCompletionRequestMessage;
      return;
    }
    if (type === 'CaptureMessage') {
      captureMessags.push([option.trim().toLowerCase(), text.trim()]);
      return;
    }
    const key = toCamelCase(name);
    if (attributes[key]) {
      requestMessages.push({ role: 'user', content: text.trim().replace('{}', attributes[key].toString()) });
    }
  });

  const mergedMessages: ChatCompletionRequestMessage[] = [];
  for (const message of requestMessages) {
    const lastMessage = mergedMessages[mergedMessages.length - 1];
    if (lastMessage && lastMessage.role === message.role) {
      lastMessage.content += message.content + '\n';
    } else {
      mergedMessages.push(message);
    }
  }

  const functionParameters = getBlogFunctionParameters(captureMessags);
  return [mergedMessages, functionParameters, repeatingInstruction];
}

export const Prompt = {
  getBlogPrompt
}