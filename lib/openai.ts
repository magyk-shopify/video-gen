import OpenAI from 'openai';
import { promises as fs } from 'fs';
import Bottleneck from "bottleneck";
import { Stream } from 'openai/streaming';
import { Logger } from './logger';
import { OpenAIImage, OpenAIImageParams } from '../types/types';
import { ImageGenerateParams } from 'openai/resources';

const openai = new OpenAI({
  organization: process.env.OPENAI_ORG_ID as string,
  apiKey: process.env.OPENAI_API_KEY as string,
  maxRetries: 5, // default is 2
});

export type ChatCompletionFunctionParamTypes = 'string' | 'array' | 'number' | 'object';


export type ChatCompletionFunctionParams = {
  type: 'object',
  properties: {
    [key: string]: {
      type: ChatCompletionFunctionParamTypes,
      items?: {
        type: ChatCompletionFunctionParamTypes
      },
      description: string
    }
  }
}

export type ChatCompletionRequestMessage = OpenAI.Chat.Completions.ChatCompletionSystemMessageParam | OpenAI.Chat.Completions.ChatCompletionUserMessageParam | OpenAI.Chat.Completions.ChatCompletionAssistantMessageParam;

export type ChatCompletionResponse = {
  choices: any
  response: OpenAI.Chat.Completions.ChatCompletion,
  request: OpenAI.Chat.Completions.ChatCompletionCreateParamsStreaming
}

export enum OpenAIModel {
  gpt3 = 'gpt-3.5-turbo',
  gpt4 = 'gpt-4',
  gpt4turbo = 'gpt-4-turbo',
  gpt4o = 'gpt-4o-2024-05-13'
}

function sanitizeForJsonParse(argument: string | undefined): string | undefined {
  if (!argument || typeof argument !== 'string') {
    return argument;
  }
  // Replace control characters with spaces.
  return argument.replaceAll('\n\n', '\\n\\n').replace(/[\u0000-\u001F]/g, ' ');
}

function parseResponse(response: OpenAI.Chat.Completions.ChatCompletion): any {
  const fArgument = response.choices?.[0].message?.tool_calls?.[0].function.arguments;
  const argument = sanitizeForJsonParse(fArgument);
  try {
    return JSON.parse(argument || 'null');
  } catch (error) {
    console.log('Failed Parsing -', argument);
    Logger.error({ error, argument }, 'Failed parsing chat completion response from OpenAI');
  }
}

async function getLocalChoices(type: string): Promise<ChatCompletionResponse> {
  const content = await fs.readFile('./config/choices.json', 'utf8');
  const choices = JSON.parse(content);
  const choice = choices.find((entry: any) => {
    return entry.type === type;
  });
  const chatCompletionRequest: OpenAI.Chat.Completions.ChatCompletionCreateParamsStreaming = choice.chatCompletionRequest;
  const chatCompletionResponse: OpenAI.Chat.Completions.ChatCompletion = choice.chatCompletionResponse;
  return {
    request: chatCompletionRequest,
    response: chatCompletionResponse,
    choices: parseResponse(chatCompletionResponse)
  };
}

async function getChoices(messages: ChatCompletionRequestMessage[], parameters: ChatCompletionFunctionParams, temperature = 0, model = 'gpt-4-turbo'): Promise<ChatCompletionResponse> {
  const tools: OpenAI.Chat.Completions.ChatCompletionTool[] = [{
    type: 'function',
    function: {
      name: 'chat_completion_response_handler',
      description: 'callback function to be called with specified parameters once response has been generated for instructions',
      parameters: parameters
    }
  }];

  const chatCompletionRequest: OpenAI.Chat.Completions.ChatCompletionCreateParamsStreaming = {
    model,
    tools,
    messages,
    temperature: temperature > 1 ? temperature / 10 : temperature,
    stream: true,
    tool_choice: { function: { name: 'chat_completion_response_handler' }, type: 'function' }
  };

  function argumentChunkToToolsCall(chunk: string, initialToolsCall: OpenAI.Chat.Completions.ChatCompletionChunk.Choice.Delta.ToolCall): OpenAI.Chat.Completions.ChatCompletionMessageToolCall {
    return {
      id: initialToolsCall.id!,
      type: initialToolsCall.type!,
      function: {
        name: initialToolsCall.function?.name!,
        arguments: chunk
      }
    };
  }

  const chatCompletionChunkStream: Stream<OpenAI.Chat.Completions.ChatCompletionChunk> = await openai.chat.completions.create(chatCompletionRequest);
  let argumentsChunk: string = '';
  let contentChunk: string = '';

  let initialChunk: OpenAI.Chat.Completions.ChatCompletionChunk | undefined;
  let finalChunk: OpenAI.Chat.Completions.ChatCompletionChunk | undefined;
  let index = 0;

  for await (const chunk of chatCompletionChunkStream) {
    const choice = chunk.choices[0];
    if (choice && choice.finish_reason === 'stop') {
      finalChunk = chunk;
    }
    if (index++ === 0) {
      initialChunk = chunk;
    }
    const [content, argument] = [choice.delta.content, choice.delta.tool_calls?.[0].function?.arguments];
    if (content) {
      contentChunk += content;
    }
    if (argument) {
      argumentsChunk += argument;
    }
  }

  if (!initialChunk || !finalChunk) {
    Logger.error({ chatCompletionRequest, contentChunk, argumentsChunk }, 'Failed retreiving chat completion stream response from OpenAI');
    throw new Error('Error in chat completion streaming api');
  }

  const { delta: { tool_calls: initialToolsCall }, logprobs, ...initialChoice } = initialChunk.choices[0];

  const choice: OpenAI.Chat.Completions.ChatCompletion.Choice = {
    ...initialChoice,
    logprobs: logprobs || null,
    finish_reason: finalChunk.choices[0].finish_reason!,
    message: {
      role: 'assistant',
      content: contentChunk,
      tool_calls: [
        argumentChunkToToolsCall(argumentsChunk, initialToolsCall?.[0]!)
      ]
    }
  };

  const chatCompletionResponse: OpenAI.Chat.Completions.ChatCompletion = { ...initialChunk, choices: [choice], object: 'chat.completion' };

  Logger.info({ chatCompletionRequest, chatCompletionResponse }, 'Processed OpenAI Chat Completion request');
  return {
    request: chatCompletionRequest,
    response: chatCompletionResponse,
    choices: parseResponse(chatCompletionResponse)
  };
}
// Create a Bottleneck limiter with a rate limit of 15 requests per minute
const dalle3limiter = new Bottleneck({
  maxConcurrent: 1,
  minTime: 4000 // 60 seconds / 15 requests = 4000 milliseconds per request
});


async function getImagesInternal({ prompt, count = 1, quality = 'hd', size = '1024x1792', style = 'natural' }: OpenAIImageParams): Promise<OpenAIImage[]> {
  const request: ImageGenerateParams = {
    prompt,
    size,
    style,
    quality,
    n: count,
    model: 'dall-e-3',
    response_format: 'url',
  };
  const response = await openai.images.generate(request);

  Logger.info({ request }, 'Processed OpenAI Image Generation request');

  const images: OpenAIImage[] = [];
  for (const entry of response.data) {
    images.push({ params: { prompt, size, quality, style, rprompt: entry.revised_prompt }, openai: entry });
  };

  return images;
}
export const getImages = dalle3limiter.wrap(getImagesInternal);

export const OpenAIChat = {
  getChoices: getChoices,
  getLocalChoices: getLocalChoices
};

export const OpenAIImages = {
  getChoices: getImages
};