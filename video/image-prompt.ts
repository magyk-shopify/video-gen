import { GoogleSheets } from '../lib/google-sheets';
import { OpenAIChat } from '../lib/openai';
import { getBlogPrompt } from '../lib/prompt';
import { ImagePromptResponse } from '../types/interfaces';

// Default Google Sheet URL
const DEFAULT_SHEET_URL =
  'https://docs.google.com/spreadsheets/d/1kNHTVBqtYTwKjeBbAgkym5igwciFHYHjctuCpBJbbFA';

export async function scriptToImagePrompt(sceneDescription: string): Promise<{ response: ImagePromptResponse }> {
  const sheetUrl = process.env.VIDEO_GOOGLE_SHEET_URL || DEFAULT_SHEET_URL;
  const template = await GoogleSheets.readTabFromSheetURL(
    sheetUrl,
    'ScriptToImagePrompt'
  );
  // Generate the prompt using the template and provided scene description
  const [messages, parameters] = getBlogPrompt(template, { sceneDescription });
  // Get choices (responses) from the OpenAI model
  const { choices } = await OpenAIChat.getChoices(messages, parameters);
  // Wrap the response in the expected format
  return { response: choices };
}