import OpenAI from 'openai';
import fs from 'fs';
import yaml from 'yaml';
import { config } from 'dotenv';

// Load environment variables
config();

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
});

// Define an interface for the expected YAML structure
interface Prompts {
  system_prompt: string;
  user_prompt: string;
}

// Read the prompt from the YAML file
const file = fs.readFileSync('./prompts.yaml', 'utf8');
const data: Prompts = yaml.parse(file) as Prompts;
const systemPrompt = data.system_prompt;
const userPrompt = data.user_prompt;

// Define the tools schema
const tools: OpenAI.Chat.ChatCompletionTool[] = [
  {
    type: 'function',
    function: {
      name: 'formatResponse',
      description: 'Formats the response into a well-structured JSON',
      parameters: {
        type: 'object',
        properties: {
          scenes: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                  scene_title: { type: 'string', description: 'Scene title' },
                  scene_description: { type: 'string', description: 'Scene description' },
                  narrator: {type: 'string', description: 'Narrator dialogues'},
                },
                required: ['scene_title', 'scene_description', 'narrator'],
            }
          }
        }
      },
    } 
  },
];

async function getChatGptResponse() {
  try {
    const response = await openai.chat.completions.create({
      model: 'gpt-3.5-turbo',
      messages: [
        { role: 'system', content: systemPrompt },
        { role: 'user', content: userPrompt },
      ],
      max_tokens: 4000,
      temperature: 0.7,
      tools: tools,
      tool_choice: { function: { name: 'formatResponse' }, type: 'function' },
    });

    console.log(response.choices[0].message.tool_calls?.[0])
    console.log(response.choices[0].message.tool_calls?.[1])
  } catch (err) {
    console.error('Error:', err);
    return err;
  }
}

getChatGptResponse();