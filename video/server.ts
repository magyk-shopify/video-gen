import * as http from 'http';
import * as fs from 'fs';
import { URL } from 'url';
import { discord } from './bot/discord-bot';
import { telegram } from './bot/tg-bot';

const hostname = '0.0.0.0';
const port = process.env.PORT ? parseInt(process.env.PORT, 10) : 3000;

const server = http.createServer(async (req, res) => {
  const url = new URL(req.url!, 'http://localhost');

  const params = url.searchParams;
  const botType = params.get('bot') || 'all';

  if (url.pathname === '/video') {
    const vpath = params.get('file');
    if (!vpath) {
      res.setHeader('Content-Type', 'text/plain');
      res.end('Not Found...\n');
      return;
    }
    const file = fs.createReadStream(vpath);
    file.on('open', () => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'video/mp4');
      file.pipe(res);
    });
    file.on('error', () => {
      res.setHeader('Content-Type', 'text/plain');
      res.end('Not Found...\n');
    });
    return;
  }

  if (url.pathname === '/start') {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    switch (botType) {
      case 'discord':
        await discord.start();
        break;
      case 'telegram':
        await telegram.start();
        break;
      case 'all':
        await discord.start();
        await telegram.start();
        break;
    }
    console.log('Bot started');
    res.end('Bot starting...\n');
    return
  }

  if (url.pathname === '/stop') {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    switch (botType) {
      case 'discord':
        await discord.stop();
        break;
      case 'telegram':
        await telegram.stop();
        break;
      case 'all':
        await discord.stop();
        await telegram.stop();
        break;
    }
    console.log('Bot stopped');
    res.end('Bot stopped\n');
    return;
  }

  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello, World!\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});