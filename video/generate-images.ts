import { getImages } from '../lib/openai';
import { ImagePrompt } from '../types/interfaces';

import fs from 'fs';
import path from 'path';
import axios from 'axios';


// Ensure the directory exists
function ensureDirectoryExists(directory: string): void {
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true });
  }
}

// Download an image from a URL and save it to the specified directory
export async function downloadImage(imageUrl: string, imageName: string, imageDir: string): Promise<string> {
  const response = await axios({
    url: imageUrl,
    method: 'GET',
    responseType: 'stream',
  });

  const imagePath = path.join(imageDir, imageName);
  await new Promise((resolve, reject) => {
    const writer = fs.createWriteStream(imagePath, {
      highWaterMark: 16 * 1024 * 1024, // 16 MB buffer size (example size)
    });
    response.data.pipe(writer);
    writer.on('finish', resolve);
    writer.on('error', reject);
  });

  return path.relative(process.cwd(), imagePath);
}

// Generate image URLs based on the provided prompts
export async function generateImageUrls(imagePrompts: ImagePrompt[], tempDir: string): Promise<string[]> {
  const imageUrls: string[] = [];

  const imageDir = path.join(tempDir, 'images');
  ensureDirectoryExists(imageDir);

  let imageIndex = 1;
  const downloadPromises: Promise<string>[] = [];

  for (const { prompt, style } of imagePrompts) {
    try {
      const images = await getImages({ prompt, style, size: '1024x1792' });

      for (const image of images) {
        const imageUrl = image.openai?.url;
        if (imageUrl) {
          const imageName = `img_${imageIndex++}.png`;
          downloadPromises.push(downloadImage(imageUrl, imageName, imageDir));
        }
      }
    } catch (error) {
      console.error(`Error generating images for prompt "${prompt}":`, error);
      throw new Error(`Failed to generate images for prompt "${prompt}".`);
    }
  }

  // Wait for all downloads to complete, throwing up any error that occurs
  await Promise.all(downloadPromises);

  // Collect successful download URLs
  downloadPromises.forEach(async (downloadPromise) => {
    const url = await downloadPromise;
    imageUrls.push(url);
  });

  return imageUrls;
}