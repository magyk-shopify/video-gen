import { storyToScript } from './story-to-script';
import { scriptToAudio } from './script-to-audio';
import { generateSubtitles } from './generate-subtitles';
import { scriptToImagePrompt } from './image-prompt';
import { downloadImage } from './generate-images';
import { createSegmentWithZoom, concatenateSegments } from './create-video';
import { Scene, ImagePromptResponse } from '../types/interfaces';
import { getImages } from '../lib/openai';
import fs from 'fs';
import * as path from 'path';
import { backOff } from 'exponential-backoff';
import * as mktemp from 'mktemp';

function ensureDirectoryExists(directory: string): void {
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true });
  }
}

function createTempdir(): string {
  const tempDir = mktemp.createDirSync('temp-XXXXXX');
  console.log(`Temporary directory created at: ${tempDir}`);
  return tempDir;
};

// Retry function (Delay is in milliseconds)
async function retry<T>(fn: (...args: any[]) => Promise<T>, args: any[], maxRetries: number, delay: number): Promise<T> {
  let attempts = 0;
  while (attempts < maxRetries) {
    try {
      return await fn(...args);
    } catch (err) {
      attempts++;
      console.error(`Error in ${fn.name}, attempt ${attempts} of ${maxRetries}:`, err);
      if (attempts < maxRetries) {
        await new Promise(resolve => setTimeout(resolve, delay));
      } else {
        throw new Error(`Failed after ${maxRetries} attempts: ${err}`);
      }
    }
  }
  // This line is unreachable, but added to satisfy TypeScript that the function will never result in an undefined state
  throw new Error('Unexpected error in retry function');
}

export async function ExecutePipeline(storyline: string): Promise<string> {
  try {

    // Generate temp folder
    const tempDir = createTempdir();

    console.log('Generating video for story -', storyline);

    // Call the storyToScript function with the content of the file
    const response = await retry(storyToScript, [storyline], 3, 2000);

    // Parse the scenes JSON string
    const scenesArray: Scene[] = JSON.parse(response.response.scenes);

    // Save scenes JSON to a file
    fs.writeFileSync(path.join(tempDir, 'scenes.json'), response.response.scenes, 'utf8');
  
    // Array to store image prompts for each scene
    const imagePrompts: ImagePromptResponse[] = [];

    // Generate audio files and subtitles for each scene
    const durations: number[] = [];
    const inputFiles: string[] = [];
    const imageDir = path.join(tempDir, 'images');
    ensureDirectoryExists(imageDir);

    // Variable to store the last successfully generated image path
    let lastSuccessfulImagePath = '';

    for (const [index, scene] of scenesArray.entries()) {
      const { narrator, scene_description } = scene;
      const audioFileName = `audio_${index + 1}`;
      let imagePath, audio, subtitle;

      try {
        // Generate audio file from narrator's dialogue
        await scriptToAudio(narrator, audioFileName, tempDir);
        audio = path.join(tempDir, 'audio', `audio_${index + 1}.mp3`)
      } catch (error) {
        console.error(`Error generating audio for scene ${index + 1}:`, error);
        throw error;
      }

      try {
        // Generate subtitles for the scene
        const subtitleFileName = `sub_${index + 1}`;
        await generateSubtitles(narrator, audioFileName, subtitleFileName, tempDir);
        //Using this format due to windows :(
        subtitle = tempDir + '/subtitles/' + `sub_${index + 1}.ass`;
      } catch (error) {
        console.error(`Error generating subtitles for scene ${index + 1}:`, error);
        throw error;
      }

      try {
        // Generate image prompt for the scene description
        const imagePromptResponse = await scriptToImagePrompt(scene_description);
        const prompt = imagePromptResponse.response.prompt;
        const style = imagePromptResponse.response.style;

        console.log('Image prompt generated successfully -', prompt);
        imagePrompts.push(imagePromptResponse.response);

        const image = await backOff(() => getImages({ prompt, style, size: '1024x1792' }), {
          startingDelay: 2000,
          maxDelay: 10000,
          numOfAttempts: 3,
          retry: (e: any) => e.code == 'content_policy_violation',
        });
        const imageUrl = image[0].openai.url;
        const imageName = `img_${index + 1}.png`;

        imagePath = await downloadImage(imageUrl, imageName, imageDir);

        // Update the last successful image path
        lastSuccessfulImagePath = imagePath;

      } catch (error: any) {
        console.error(`Error generating image prompt for scene ${index + 1}:`, error);
        // If the first image fails, stop the video:
        if (index == 0) throw error;

        // Use the last successful image as a fallback
        imagePath = lastSuccessfulImagePath;
      }

      try {
        // Generate segment for the scene
        const segmentOutput = path.join(tempDir, `segment${index}.mp4`);
        const duration = await createSegmentWithZoom(imagePath, audio, subtitle, segmentOutput);
        inputFiles.push(segmentOutput);
        durations.push(duration);
      }
      catch (error) {
        console.error(`Error generating segment for scene ${index + 1}:`, error);
        throw error;
      }

    }

    // Save image prompts JSON to a file
    fs.writeFileSync(path.join(tempDir, 'image-prompts.json'), JSON.stringify(imagePrompts), 'utf8');

    // Output file path for the video
    const outputFilePath = path.join(tempDir, 'AIvideo.mp4');

    // Create video with transitions
    await concatenateSegments(inputFiles, durations, outputFilePath);

    console.log(`Video created successfully at ${outputFilePath}`);

    // Clean up temporary files and directory
    // fs.rmSync(tempDir, { recursive: true, force: true });
    // console.log(`Temporary directory and its contents have been removed.`);

    return outputFilePath;
  } catch (err) {
    console.error('Pipeline execution failed:', err);
    throw err; // Re-throw the error to stop further execution
  }
}

//example usage
// ExecutePipeline('In the silent forest, an old oak whispered secrets to the wind. One night, a lost traveler listened closely and heard a hidden path.');