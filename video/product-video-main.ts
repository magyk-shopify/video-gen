import OpenAI from 'openai';
import { config } from 'dotenv';
import fs from 'fs';
import * as path from 'path';
import { createSegmentWithZoom, concatenateSegments } from './create-video'; 
import { Scene, ImagePromptResponse } from '../types/interfaces';
import { backOff } from 'exponential-backoff';
import * as mktemp from 'mktemp';
import { storyToScript } from './story-to-script';
import { scriptToAudio } from './script-to-audio';
import { generateSubtitles } from './generate-subtitles';
import { scriptToImagePrompt } from './image-prompt';
import { downloadImage } from './generate-images';

config();
const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
});

const descriptionToScriptPersona = `
You are an expert in product narration, specializing in conveying detailed features and benefits to potential users and buyers. 
Your expertise lies in articulating the unique qualities and practical applications of various products, ensuring listeners understand their value and functionality. 
You excel in highlighting key attributes and uses, providing insights that resonate with diverse consumer needs and preferences.
`;

function descriptionToPrompt({ description, images }: { description: string, images: string[] }): string {
  const imageDescriptions = images.map((image, index) => `Image ${index + 1}: ${path.basename(image)}`).join('\n');
  return `
Below is a detailed description of a product along with a series of images. Write a compelling narration that captures the unique features, benefits, and practical applications of the product, appealing to potential buyers or users. Ensure your narration is engaging and informative, highlighting the product's key attributes and why it stands out in its category.

Description:
${description}

Images:
${imageDescriptions}
`;
}

export async function descriptionToStory(description: string, images: string[]): Promise<string> {
  const params = {
    messages: [
      { role: 'system', content: descriptionToScriptPersona },
      { role: 'user', content: descriptionToPrompt({ description, images }) }
    ],
    model: 'gpt-4-turbo-preview',
  };
  const response = await openai.chat.completions.create(params);
  console.log('DescriptionToScript: Generated Script', JSON.stringify({ request: params, response }));
  return response.choices[0].message.content!;
}

function ensureDirectoryExists(directory: string): void {
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true });
  }
}

function createTempdir(): string {
  const tempDir = mktemp.createDirSync('temp-XXXXXX');
  console.log(`Temporary directory created at: ${tempDir}`);
  return tempDir;
}

// Retry function (Delay is in milliseconds)
async function retry<T>(fn: (...args: any[]) => Promise<T>, args: any[], maxRetries: number, delay: number): Promise<T> {
  let attempts = 0;
  while (attempts < maxRetries) {
    try {
      return await fn(...args);
    } catch (err) {
      attempts++;
      console.error(`Error in ${fn.name}, attempt ${attempts} of ${maxRetries}:`, err);
      if (attempts < maxRetries) {
        await new Promise(resolve => setTimeout(resolve, delay));
      } else {
        throw new Error(`Failed after ${maxRetries} attempts: ${err}`);
      }
    }
  }
  throw new Error('Unexpected error in retry function');
}

const preDownloadedImages = [
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image1.png',
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image2.png',
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image3.png',
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image4.png',
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image5.png',
];

export async function ExecutePipeline(storyline: string): Promise<string> {
  try {
    const tempDir = createTempdir();

    console.log('Generating video for story -', storyline);

    const maxDuration = 45;
    const numImages = preDownloadedImages.length;
    const response = await retry(storyToScript, [storyline, maxDuration, numImages], 3, 2000);

    const scenesArray: Scene[] = JSON.parse(response.response.scenes);

    fs.writeFileSync(path.join(tempDir, 'scenes.json'), response.response.scenes, 'utf8');
  
    const imagePrompts: ImagePromptResponse[] = [];

    const durations: number[] = [];
    const inputFiles: string[] = [];
    const imageDir = path.join(tempDir, 'images');
    ensureDirectoryExists(imageDir);

    for (const [index, scene] of scenesArray.entries()) {
      const { narrator, scene_description } = scene;
      const audioFileName = `audio_${index + 1}`;
      let imagePath, audio, subtitle;

      try {
        await scriptToAudio(narrator, audioFileName, tempDir);
        audio = path.join(tempDir, 'audio', `audio_${index + 1}.mp3`);
      } catch (error) {
        console.error(`Error generating audio for scene ${index + 1}:`, error);
        throw error;
      }

      try {
        const subtitleFileName = `sub_${index + 1}`;
        await generateSubtitles(narrator, audioFileName, subtitleFileName, tempDir);
        subtitle = path.join(tempDir, 'subtitles', `sub_${index + 1}.ass`);
      } catch (error) {
        console.error(`Error generating subtitles for scene ${index + 1}:`, error);
        throw error;
      }

      try {
        imagePath = preDownloadedImages[index];
        if (!imagePath) {
          throw new Error(`No pre-downloaded image available for scene ${index + 1}`);
        }
      } catch (error) {
        console.error(`Error fetching image for scene ${index + 1}:`, error);
        throw error;
      }

      try {
        const segmentOutput = path.join(tempDir, `segment${index}.mp4`);
        const duration = await createSegmentWithZoom(imagePath, audio, subtitle, segmentOutput);
        inputFiles.push(segmentOutput);
        durations.push(duration);
      } catch (error) {
        console.error(`Error generating segment for scene ${index + 1}:`, error);
        throw error;
      }
    }

    fs.writeFileSync(path.join(tempDir, 'image-prompts.json'), JSON.stringify(imagePrompts), 'utf8');

    const outputFilePath = path.join(tempDir, 'AIvideo.mp4');

    await concatenateSegments(inputFiles, durations, outputFilePath);

    console.log(`Video created successfully at ${outputFilePath}`);

    // Uncomment this to clean up temporary files
    // fs.rmSync(tempDir, { recursive: true, force: true });
    // console.log(`Temporary directory and its contents have been removed.`);

    return outputFilePath;
  } catch (err) {
    console.error('Pipeline execution failed:', err);
    throw err;
  }
}

(async () => {
  const productDescription = 'Kevin Levrone Anabolic Mass Gainer 3 Kg (Chocolate)';
  const preDownloadedImages = [
    'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image1.png',
    'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image2.png',
    'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image3.png',
    'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image4.png',
    'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image5.png',
  ];
  try {
    const story = await descriptionToStory(productDescription, preDownloadedImages);
    const videoPath = await ExecutePipeline(story);
    console.log('Video created at:', videoPath);
  } catch (error) {
    console.error('Error:', error);
  }
})();
