import { GoogleSheets } from '../lib/google-sheets';
import { OpenAIChat } from '../lib/openai';
import { getBlogPrompt } from '../lib/prompt';
import { ScriptResponse } from '../types/interfaces';
import { descriptionToStory } from './product-video-main';

// const DEFAULT_SHEET_URL =
//   'https://docs.google.com/spreadsheets/d/1kNHTVBqtYTwKjeBbAgkym5igwciFHYHjctuCpBJbbFA';

const DEFAULT_SHEET_URL =
  'https://docs.google.com/spreadsheets/d/1S13UOT7HqD1OEPwgMf7D8NdgL44Jj2FojRRDjua6_6s';

export async function storyToScript(storyline: string): Promise<{ response: ScriptResponse }> {
  const sheetUrl = process.env.VIDEO_GOOGLE_SHEET_URL || DEFAULT_SHEET_URL;
  const template = await GoogleSheets.readTabFromSheetURL(
    sheetUrl,
    'StoryToScript'
  );
  const [messages, parameters] = getBlogPrompt(template, { storyline });
  const { choices } = await OpenAIChat.getChoices(messages, parameters);
  return { response: choices };
}

const preDownloadedImages = [
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image1.png',
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image2.png',
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image3.png',
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image4.png',
  'C:/Users/SIDDU/shopify/whey_protien_images/whey_protien_image5.png',
];

const story = descriptionToStory('Kevin Levrone Anabolic Mass Gainer 3 Kg (Chocolate)', preDownloadedImages);
// console.log(story);

// export async function storyToScript(storyline: string, imagePaths: string[]): Promise<{ response: ScriptResponse }> {
//   const sheetUrl = process.env.VIDEO_GOOGLE_SHEET_URL || DEFAULT_SHEET_URL;
//   const template = await GoogleSheets.readTabFromSheetURL(sheetUrl, 'StoryToScript');

//   // Modify the template to include image paths
//   const modifiedTemplate = template.replace('"{imagePaths}"', JSON.stringify(imagePaths));

//   const [messages, parameters] = getBlogPrompt(modifiedTemplate, { storyline });
//   const { choices } = await OpenAIChat.getChoices(messages, parameters);
//   return { response: choices };
// }



storyToScript(story)
  .then(response => console.log(response))
  .catch(error => console.error(error));

// async function main() {
//   try {
//     // Read the content of the input story file
//     const inputFilePath = path.join(__dirname, 'story.txt');
//     const storyline = fs.readFileSync(inputFilePath, 'utf8').trim();

//     // Call the storyToScript function with the content of the file
//     const response = await storyToScript(storyline);

//     // Parse the scenes JSON string
//     const scenesArray: Scene[] = JSON.parse(response.response.scenes);
//     // Output the scenesArray which is the input for the next function
//     console.log(scenesArray);
//   } catch (err) {
//     console.error('Error reading input file:', err);
//   }
// }

// main();