import fs from 'fs';
import path from 'path';
import OpenAI from 'openai';
import dotenv from 'dotenv';
import { Transcription, Segment } from '../types/interfaces';

dotenv.config();

interface APIResponse extends Response {
  arrayBuffer(): Promise<ArrayBuffer>;
}

interface OpenAIConfig {
  apiKey: string;
}

const TEMP_DIR = path.join(__dirname, 'temp');
const AUDIO_DIR = path.join(TEMP_DIR, 'audio');

function validateEnvVariables(): void {
  if (!process.env.OPENAI_API_KEY) {
    throw new Error('OPENAI_API_KEY is not defined in the environment variables.');
  }
}

function createOpenAIInstance(): OpenAI {
  const config: OpenAIConfig = {
    apiKey: process.env.OPENAI_API_KEY as string,
  };
  return new OpenAI(config);
}

function ensureDirectoryExists(directory: string): void {
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true });
  }
}

async function getAudioBuffer(text: string, openai: OpenAI): Promise<Buffer> {
  const mp3: APIResponse = await openai.audio.speech.create({
    model: 'tts-1',
    voice: 'fable',
    input: text,
  });

  if (!mp3) {
    throw new Error('Failed to get a response from OpenAI API.');
  }

  return Buffer.from(await mp3.arrayBuffer());
}

async function saveAudioFile(buffer: Buffer, filePath: string): Promise<void> {
  await fs.promises.writeFile(filePath, buffer);
}

export async function scriptToAudio(text: string, audioFileName: string, tempDir: string): Promise<Segment[]> {
  let words: Segment[] = [];
  try {
    validateEnvVariables();
    const openai = createOpenAIInstance();

    const audioDir = path.join(tempDir, 'audio');
    ensureDirectoryExists(audioDir);

    const buffer = await getAudioBuffer(text, openai);
    const speechFile = path.join(audioDir, `${audioFileName}.mp3`);
    await saveAudioFile(buffer, speechFile);

    console.log('Speech file created successfully:', speechFile);

    // Make the transcription request to OpenAI API
    const transcription = (await openai.audio.transcriptions.create({
      file: fs.createReadStream(path.join(audioDir, `${audioFileName}.mp3`)),
      model: 'whisper-1',
      response_format: 'verbose_json',
      timestamp_granularities: ['word'],
    })) as Transcription;
    words = transcription.words;

  } catch (error) {
    console.error('Error:', error);
  }
  return words;
}

// Example usage
// scriptToAudio('As the sun sets, painting the sky crimson, Ashwatthama stands amidst the devastation of Kurukshetra, his heart ablaze with a vengeful fire.', 'The_Night_of_Revenge');