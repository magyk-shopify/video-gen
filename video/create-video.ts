import * as fs from 'fs';
import * as path from 'path';
import { spawn, exec } from 'child_process';
import { Media } from '../types/interfaces';

const PROJECT_DIR = path.join(__dirname);
const FFMPEG_PATH = process.env.APP_ENV === 'development' ? 'ffmpeg' : path.join(PROJECT_DIR, '../.cache/ffmpeg/ffmpeg');

// Ensure the directory exists
function ensureDirectoryExists(directory: string): void {
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true });

  }
  fs.chmodSync(directory, 0o777);
}

// Get the duration of an audio file
export function getAudioDuration(filePath: string): Promise<number> {
  return new Promise((resolve, reject) => {
    exec(`${FFMPEG_PATH} -i "${filePath}" 2>&1 | grep "Duration"`, (error, stdout) => {
      if (error) {
        return reject(error);
      }

      const durationMatch = stdout.match(/Duration: (\d+):(\d+):(\d+\.\d+)/);
      if (!durationMatch) {
        return reject(new Error('Could not parse duration'));
      }

      const hours = parseInt(durationMatch[1], 10);
      const minutes = parseInt(durationMatch[2], 10);
      const seconds = parseFloat(durationMatch[3]);

      const totalDuration = hours * 3600 + minutes * 60 + seconds;
      resolve(totalDuration);
    });
  });
}

// Generate the filter_complex string for ffmpeg
function generateFilterComplex(numFiles: number, durations: number[]): string {
  let filterComplex = '';
  const transition = 'fade';
  const tDuration = 1;
  let offset = durations[0] - tDuration;

  for (let i = 0; i < numFiles - 1; i++) {
    if (i === 0) {
      filterComplex += `[0:v][1:v]xfade=transition=${transition}:duration=${tDuration}:offset=${offset}[v1];`;
    } else {
      filterComplex += `[v${i}][${i + 1}:v]xfade=transition=${transition}:duration=${tDuration}:offset=${offset}[v${i + 1}];`;
    }
    offset += durations[i + 1] - tDuration;
  }

  for (let i = 0; i < numFiles; i++) {
    filterComplex += `[${i}:a]`;
  }
  filterComplex += `concat=n=${numFiles}:v=0:a=1[a${numFiles - 1}]`;
  return filterComplex;
}

// Execute ffmpeg command
function executeFfmpeg(args: string[]): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    const command = `${FFMPEG_PATH} -loglevel error ${args.join(' ')}`;
    console.log(`Executing command: ${command}`);

    const ffmpegProcess = spawn(FFMPEG_PATH!, args, { shell: true });

    ffmpegProcess.stdout.on('data', (data) => {
      console.log(`stdout: ${data}`);
    });

    ffmpegProcess.stderr.on('data', (data) => {
      console.error(`stderr: ${data}`);
    });

    ffmpegProcess.on('close', (code) => {
      if (code === 0) {
        resolve();
      } else {
        reject(new Error(`ffmpeg process exited with code ${code}`));
      }
    });
  });
}

// Concatenate video segments with crossfade transitions
export async function concatenateSegments(inputFiles: string[], durations: number[], outputFilePath: string): Promise<void> {
  const filterComplex = generateFilterComplex(inputFiles.length, durations);

  const args = [
    '-max_alloc', '100000000',
    ...inputFiles.flatMap((file) => ['-i', file]),
    '-filter_complex', `"${filterComplex}"`,
    '-pix_fmt', 'yuv420p',
    '-c:v', 'libx264',
    '-y',
    '-map', `[v${inputFiles.length - 1}]`,
    '-map', `[a${inputFiles.length - 1}]`,
    outputFilePath,
    '-c:a', 'copy',
  ];

  await executeFfmpeg(args);
}

// Create a segment from image, audio, and subtitle
async function createSegment(image: string, audio: string, subtitle: string, output: string): Promise<number> {
  const audioDuration = await getAudioDuration(audio);
  if (audioDuration === undefined) {
    throw new Error(`Failed to retrieve duration for audio: ${audio}`);
  }

  const args = [
    '-max_alloc', '100000000',
    '-loop', '1',
    '-i', image,
    '-i', audio,
    '-c:v', 'libx264',
    '-c:a', 'aac',
    '-strict', 'experimental',
    '-pix_fmt', 'yuv420p',
    '-t', (audioDuration + 1).toString(), // Extend image duration by 1 second
    '-vf', `"ass=${subtitle}"`,
    '-y',
    output,
  ];

  await executeFfmpeg(args);

  return audioDuration + 1; // Return the extended duration
}

// Note:
// - Currenty image dimensions are hard coded here. Things will only work for 1024x1792 images
// - The scale 4000 thing is a workaround for avoiding jitter that ffmpeg seems to produces when zooming
//   Forcing it to work in larger pixed dimensions seems smooth things out in its calculations.
const zoom_filter_complex = `[0:v]scale=4000:-1,zoompan=z='zoom+0.001':x='iw/2-(iw/zoom/2)':y='ih/2-(ih/zoom/2)':d=250:s=540x960:fps=25`

export async function createSegmentWithZoom(image: string, audio: string, subtitle: string, output: string): Promise<number> {
  console.log("Creating segment with image:", image, "audio:", audio, "subtitle:", subtitle, "output:", output)

  const audioDuration = await getAudioDuration(audio);
  if (audioDuration === undefined) {
    throw new Error(`Failed to retrieve duration for audio: ${audio}`);
  }

  const args = [
    '-max_alloc', '100000000',
    '-loop', '1',
    '-i', image,
    '-i', audio,
    '-c:v', 'libx264',
    '-c:a', 'aac',
    '-filter_complex', `${zoom_filter_complex},ass=${subtitle}[out]`,
    '-map', '"[out]"',
    '-map', '1:a',
    '-pix_fmt', 'yuv420p',
    '-r', '25',
    '-t', (audioDuration + 1).toString(), // Extend image duration by 1 second
    '-y',
    output,
  ];

  await executeFfmpeg(args);

  return audioDuration + 1; // Return the extended duration
}

// Create concat list file
function createConcatListFile(inputFiles: string[], tempDir: string): string {
  const concatList = inputFiles.map(file => `file '${file}'`).join('\n');
  const concatFilePath = path.join(tempDir, 'concat.txt');
  fs.writeFileSync(concatFilePath, concatList);
  return concatFilePath;
}

// Create a video from a list of media files
export async function createVideo(mediaList: Media[], outputFilePath: string, tempDir: string): Promise<void> {

  const inputFiles: string[] = [];
  const durations: number[] = [];

  for (let i = 0; i < mediaList.length; i++) {
    const { image, audio, subtitle } = mediaList[i];
    const segmentOutput = path.join(tempDir, `segment${i}.mp4`);

    try {
      const duration = await createSegmentWithZoom(image, audio, subtitle, segmentOutput);
      inputFiles.push(segmentOutput);
      durations.push(duration);
    } catch (error) {
      if (error instanceof Error) {
        console.error(error.message);
      } else {
        console.error('An unknown error occurred');
      }
      continue;
    }
  }

  await concatenateSegments(inputFiles, durations, outputFilePath);

  console.log(`Video created at ${outputFilePath}`);
}