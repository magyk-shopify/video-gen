import fs from 'fs';
import OpenAI from 'openai';
import path from 'path';
import { Segment, Transcription } from '../types/interfaces';

// Initialize the OpenAI API client
const openai = new OpenAI();


// Helper function to format time for ASS
function formatTime(seconds: number): string {
  const hours: number = Math.floor(seconds / 3600);
  seconds %= 3600;
  const minutes: number = Math.floor(seconds / 60);
  seconds %= 60;
  const milliseconds: number = Math.round((seconds - Math.floor(seconds)) * 100); // ASS uses centiseconds
  seconds = Math.floor(seconds);
  return `${hours.toString().padStart(1, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}.${milliseconds.toString().padStart(2, '0')}`;
}

// Ensure the directory exists
function ensureDirectoryExists(directory: string): void {
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true });
  }
}

function fixSubtitlePunctuations(original: string, substring: string): string {
  const wordRegex = /(\w+)(\W*)/g;
  const originalWords = [...original.matchAll(wordRegex)].map(match => ({
    word: match[1],
    punct: match[2]
  }));

  const subWords = substring.split(/\s+/);
  const replacedWords = subWords.map(subWord => {
    const match = originalWords.find(
      originalWord => originalWord.word.toLowerCase() === subWord.toLowerCase()
    );
    return match ? match.word + match.punct : (subWord + ' ');
  });
  return replacedWords.join('');
}

// Create ASS content from word-level timestamps
function createAssContent(narration: string, words: Segment[], maxWordsPerSegment: number): string {
  let assContent = `
[Script Info]
Title: Generated ASS
ScriptType: v4.00+
Collisions: Normal
PlayDepth: 0

[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding
Style: Default,Inter,11,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,1,0,0,0,100,100,0,0,1,1,0,2,10,10,10,1

[Events]
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text
`;

  for (let i = 0; i < words.length; i += maxWordsPerSegment) {
    const subSegmentWords = words.slice(i, i + maxWordsPerSegment);
    const subSegmentText = subSegmentWords.map(word => word.word).join(' ');
    const subSegmentStartTime = subSegmentWords[0].start;
    const subSegmentEndTime = subSegmentWords[subSegmentWords.length - 1].end;

    const startTime = formatTime(subSegmentStartTime);
    const endTime = formatTime(subSegmentEndTime);

    assContent += `Dialogue: 0,${startTime},${endTime},Default,,0,0,0,,${fixSubtitlePunctuations(narration, subSegmentText)}\n`;
  }

  return assContent;
}

// Write ASS content to a file
function writeAssFile(filePath: string, content: string): void {
  fs.writeFileSync(filePath, content);
}

/**
 * Generate subtitles for an audio file and save the ASS file in the temp/subtitles folder.
 * @param audioFileName - The name of the audio file.
 * @param subtitleFileName - The name of the subtitle file.
 * @param tempDir - The temporary directory to use for storing files.
 */
export async function generateSubtitles(narration: string, audioFileName: string, subtitleFileName: string, tempDir: string): Promise<void> {
  try {
    const subtitlesDir = path.join(tempDir, 'subtitles');
    const audioDir = path.join(tempDir, 'audio');
    ensureDirectoryExists(subtitlesDir);

    // Make the transcription request to OpenAI API
    const transcription = (await openai.audio.transcriptions.create({
      file: fs.createReadStream(path.join(audioDir, `${audioFileName}.mp3`)),
      model: 'whisper-1',
      response_format: 'verbose_json',
      timestamp_granularities: ['word'],
    })) as Transcription;

    //console.log(transcription);

    const assContent = createAssContent(narration, transcription.words, 4);
    const outputFilePath = path.join(subtitlesDir, `${subtitleFileName}.ass`);
    writeAssFile(outputFilePath, assContent);

    console.log(`ASS file created successfully: ${outputFilePath}`);
  } catch (error) {
    console.error('Error:', error);
  }
}

// Example usage
// generateSubtitles('Lastly, Smart Snacking. Choose nutrient-rich snacks over processed options to keep your energy levels stable throughout the day.', 'a4', 'as4', './temp');
// console.log(fixSubtitlePunctuations('Next, practice portion control. Use smaller plates to help reduce the amount of food you eat without feeling deprived.', 'next practise portion control'));