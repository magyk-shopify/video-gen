import { Bot, InputFile } from 'grammy';
import dotenv from 'dotenv';
import { ExecutePipeline } from '../video-main';

// Load environment variables from .env file
dotenv.config();

// Replace with your actual Telegram bot token
if (!process.env.TELEGRAM_BOT_TOKEN) {
  throw new Error('TELEGRAM_BOT_TOKEN is not defined in the environment variables');
}

const activeRequests: { [key: number]: boolean } = {};

const bot = new Bot(process.env.TELEGRAM_BOT_TOKEN as string);
bot.api.setMyCommands([
  { command: 'start', description: 'Description To Video' }
])

bot.command('start', async (context) => {
  const chatId = context.chat?.id;
  console.log('Telegram bot interaction started');
  bot.api.sendMessage(chatId, 'Provide description to make video', { reply_markup: { force_reply: true } });
});

bot.on('message:text', async (context) => {
  const message = context.message;
  const chatId = context.chat?.id;
  const userId = context.from?.id;
  const text = message.text.trim();

  if (!text.startsWith('#magyk')) {
    bot.api.sendMessage(chatId, 'Access Denied');
    return;
  }

  if (activeRequests[userId]) {
    bot.api.sendMessage(chatId, 'Video generation in progress. Please wait for the current video to be generated.');
    return;
  }

  const story = text.replace('#magyk', '').trim();
  if (!story) {
    bot.api.sendMessage(chatId, 'No text provided. Please try again.');
    return;
  }

  bot.api.sendMessage(chatId, 'Generating video... Please be patient');

  activeRequests[userId] = true;

  console.log('Video generation started');

  //bot.api.sendMessage(chatId, `Working files can be found at : https://magyk-video-bot.onrender.com/video?file=./${tempDir}/`);

  try {
    const videoFilePath = await ExecutePipeline(story);
    await bot.api.sendMessage(chatId, 'Video generated successfully.');
    await bot.api.sendVideo(chatId, new InputFile(videoFilePath));

  } catch (error) {
    await bot.api.sendMessage(chatId, 'Video generation failed!');
    console.log(error);
  } finally {
    activeRequests[userId] = false;
  }
});

export const telegram = {
  start: async () => {
    await bot.start();
  },
  stop: async () => {
    await bot.stop();
  }
};