import { Client, GatewayIntentBits, TextChannel } from 'discord.js';
import dotenv from 'dotenv';
import { ExecutePipeline } from '../video-main';
import { log } from 'console';

// Load environment variables from .env file
dotenv.config();

// Replace with your actual Discord bot token
if (!process.env.DISCORD_BOT_TOKEN) {
  throw new Error('DISCORD_BOT_TOKEN is not defined in the environment variables');
}

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.MessageContent
  ]
});

const activeRequests: { [key: string]: boolean } = {};

client.once('ready', () => {
  console.log('Discord bot is online!');
});

client.on('messageCreate', async (message: any) => {
  if (message.author.bot) return;

  const userId = message.author.id;
  let text = message.content.trim();

  // Check if the message is a DM or if the bot is mentioned
  const isDM = !message.guildId;
  const isBotMentioned = message.mentions.users.has(client?.user?.id);

  if (!isDM && !isBotMentioned) {
    return;
  }

  if (isBotMentioned) {
    const botMention = client.user?.displayName;
    text = text.replace(botMention, '').trim();
  }

  // If the bot is mentioned, remove the mention from the text
  if (isBotMentioned) {
    const botMention = /<@\d+>/;
    text = text.replace(botMention, '').trim();
  }

  if (text === '!start') {
    message.reply('Provide a description to generate a video');
    return;
  }

  if (!text.startsWith('#magyk')) {
    message.reply('Access Denied');
    return;
  }

  if (activeRequests[userId]) {
    message.reply('Video generation in progress. Please wait for the current video to be generated.');
    return;
  }

  const story = text.replace('#magyk', '').trim();

  if (!story) {
    message.reply('No text provided. Please try again.');
    return;
  }

  message.reply('Generating video... Please be patient');
  console.log('Video generation started');
  activeRequests[userId] = true;

  try {
    const videoFilePath = await ExecutePipeline(story);
    message.reply('Video generated successfully.');
    const channel = message.channel as TextChannel;
    await channel.send({
      files: [videoFilePath]
    });
  } catch (error) {
    message.reply('Video generation failed!');
    console.log(error);
  } finally {
    activeRequests[userId] = false;
  }
});

export const discord = {
  start: async () => {
    await client.login(process.env.DISCORD_BOT_TOKEN);
  },
  stop: async () => {
    await client.destroy();
  }
};