const fs = require('fs');
const childProcess = require('child_process');
const path = require('path');

// Get the project root directory
const projectRoot = process.cwd();
const cacheDir = path.join(projectRoot, '.cache');
const ffmpegBinary = path.join(cacheDir, 'ffmpeg', 'ffmpeg');

console.log('Checking for FFmpeg...\n');

console.log('FFmpeg binary expected at:', ffmpegBinary);
console.log('File exists:', fs.existsSync(ffmpegBinary));

if (!fs.existsSync(ffmpegBinary)) {
  console.log('FFmpeg not found, downloading...\n');

  const command = `
    mkdir -p ${cacheDir} &&
    wget -O ${path.join(cacheDir, 'ffmpeg.zip')} https://storage.googleapis.com/magyk-sw/ffmpeg.zip &&
    unzip -oq ${path.join(cacheDir, 'ffmpeg.zip')} -d ${cacheDir} &&
    rm ${path.join(cacheDir, 'ffmpeg.zip')}
  `;

  try {
    childProcess.execSync(command, { stdio: 'inherit' });
    console.log('FFmpeg installed successfully.\n');
  } catch (e) {
    console.error('FFmpeg installation failed -', e.message);
  }
} else {
  console.log('FFmpeg is already installed.\n');
}